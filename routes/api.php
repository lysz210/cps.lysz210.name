<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('', function() {
    return [
        'id' => time(),
        'data' => '17-08-2019',
        'stato' => [
            'it' => 'test',
            'en' => 'test...'
        ]
    ];
});

Route::get('check', 'ControllaPermessoSoggiorno@check');

Route::prefix('i18n')->group(function () {
    Route::get('locales', function () {
        return LaravelLocalization::getLocalesOrder();
    });

    Route::get('{locale}/{group}', function ($locale, $group) {
        return \Lang::get($group, [], $locale);
    });
});