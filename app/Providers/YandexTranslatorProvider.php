<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Utils\Yandex\Translator;

class YandexTranslatorProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Translator::class, function () {
            return new Translator(config('yandex.translator.key'));
        });
    }
}
