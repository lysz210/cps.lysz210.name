<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TranslationConfig extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('laravellocalization.syncWithDB') && \Schema::hasTable('translator_languages')) {
            $langs = \DB::table('translator_languages')->get()->mapWithKeys(function ($item) {
                return [$item->locale => (array)$item];
            })->toArray();
            if (count($langs)) {
                config([
                    'laravellocalization.supportedLocales' => $langs
                ]);
                \LaravelLocalization::setSupportedLocales($langs);
            }
        }
    }
        
    /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {
            //
    }
}
