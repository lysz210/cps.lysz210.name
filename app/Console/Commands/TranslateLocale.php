<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \Waavi\Translation\Repositories\LanguageRepository;
use \Waavi\Translation\Repositories\TranslationRepository;
use App\Utils\Yandex\Translator;

class TranslateLocale extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:locale {group} {locale?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Translate the locale on fallback language';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        \App $app, LanguageRepository $languageRepository,
        TranslationRepository $translationRepository,
        Translator $yTranslator
    )
    {
        parent::__construct();
        $this->languageRepo = $languageRepository;
        $this->translationRepo = $translationRepository;
        $this->app = $app;
        $this->yTranslator = $yTranslator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (false && !$this->confirm('Attenzione la traduzione puo creare problemi alle variabili nei template, proseguire?')) {
            return;
        }
        $group = $this->argument('group');
        $srcLocale = $this->argument('locale') ? $this->argument('locale') : \Lang::getFallback();
        $path = implode(DIRECTORY_SEPARATOR, [$srcLocale, $group]) . '.php';
        $translationStorage = \Storage::disk('translations');
        if ($translationStorage->exists($path)) {
            $fullPath = $translationStorage->path($path);
            $dictionary = array_dot(\File::getRequire($fullPath));
            $locales = $this->languageRepo->allExcept($srcLocale)->pluck('locale');
            foreach ($locales as $destLocale) {
                $translated = [];
                foreach ($dictionary as $item => $text) {
                    $tText = $this->yTranslator->translate($text, $destLocale, $srcLocale);
                    if ($tText === $text) {
                        $this->info('La traduzione tornata e identica');
                    } else {
                        $this->info('frase tradotta: ' . $tText);
                        $translated[$item] = $tText;
                    }
                }
                $this->translationRepo->loadArray($translated, $destLocale, $group);
                sleep(15);
            }
            // $record = $this->translationRepo->untranslated($locale);
            // dd($translated);
            \TranslationCache::flushAll();
        } else {
            $this->error("Non esiste nessun file di lingua $group nella lingua di default!!!");
        }
    }
}
