<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FacebookWebhook extends Controller
{
    protected $pageToken;
    function _constructor() {
        $this->pageToken = env('PAGE_TOKEN');
    }

    function subscribe(Request $req) {
        // dd($req->all());
        if ($req->query('hub_mode') === 'subscribe' && $req->query('hub_verify_token') === env('VERIFY_TOKEN')) {
            return $req->query('hub_challenge');
        } else {
            throw new \Exception('Failed validation. Make sure the validation tokens match.');
        }
    }

    function listen(Request $req) {
        if ($req->input('object') === 'page') {
            
        }
    }
}
