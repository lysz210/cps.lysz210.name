<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client as HttpClient;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Mcamara\laravelLocalization\Facades\LaravelLocalization;
use App\Utils\Yandex\Translator;

class ControllaPermessoSoggiorno extends Controller
{
    protected $_baseQuery = [
        'lang' => 'italian',
        'invia' => 'invia',
        'mime' => '4',
        'pratica' => ''
    ];

    protected $_httpConfigs = [
        'base_uri' => 'http://questure.poliziadistato.it'
    ];

    protected $_translator;

    function __construct(Translator $translator) {
        $this->_translator = $translator;
    }

    protected function trans($text, $lang = 'en', $srcLang = 'it') {
        if ($srcLang == $lang) {
            return $text;
        }

        return $this->_translator->translate($text, $lang, $srcLang);
    }

    function show () {
        return view('pages.home');
    }
    
    function controlla (Request $req) {
        $pratica = $req->query('pratica');
        return view('pages.controlla', [
            'pratica' => $this->_getFeed($pratica)
            ]);
    }

    function check (Request $req) {
        $pratica = $req->query('pratica');
        if ($pratica) {
            $q = $this->_baseQuery;
            $q['pratica'] = $pratica;
            $http = new HttpClient($this->_httpConfigs);
            $response = $http->get('stranieri', ['query' => $q]);
            // dd($response);
            if ($response->getStatusCode() == 200) {
                $body = (string)$response->getBody();
                $body = str_replace('&pratica', '&amp;pratica', $body);
                $dom = new DomCrawler($body);
                $descrizione = trim($dom->filter('item > description')->text());
                $mapDescrizione = [
                    'it' => $descrizione
                ];
                $currentLocale = LaravelLocalization::getCurrentLocale();
                if ($currentLocale != 'it') {
                    $mapDescrizione[$currentLocale] = $this->trans($descrizione, $currentLocale);
                }
                // dd($dom);
                // restituire json con risposta originale e tradotto
                // se lingua di traduzione e' italiano non serve la traduzione
                return [
                    'id' => $pratica,
                    'descrizione' => $mapDescrizione,
                    'data' => trim($dom->filter('item > pubDate')->text()),
                ];
            } else {
                return null;
            }
        } else return null;
    }
    
    protected function _getFeed ($pratica) {
        if ($pratica) {
            $q = $this->_baseQuery;
            $q['pratica'] = $pratica;
            $http = new HttpClient($this->_httpConfigs);
            $response = $http->get('stranieri', ['query' => $q]);
            // dd($response);
            if ($response->getStatusCode() == 200) {
                $body = (string)$response->getBody();
                $body = str_replace('&pratica', '&amp;pratica', $body);
                $dom = new DomCrawler($body);
                // dd($dom);
                return [
                    'id' => $pratica,
                    'descrizione' => $this->trans(trim($dom->filter('item > description')->text()), LaravelLocalization::getCurrentLocale()),
                    'data' => trim($dom->filter('item > pubDate')->text()),
                ];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
