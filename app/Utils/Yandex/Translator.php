<?php

namespace App\Utils\Yandex;

use GuzzleHttp\Client as HttpClient;

class Translator {

    const API_KEY_PATTERN = "/^trnsl[a-zA-Z0-9.]+$/";

    const DIRECTION_FORMAT = "%s-%s";
    
    const DEFAULT_SOURCE_LANG = "en";
    private $_apiKey;

    private $_url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    private $_defaultSrcLang = 'en';

    private $_httpClient;

    function __construct(string $apiKey) {
        if (!preg_match(self::API_KEY_PATTERN, $apiKey)) {
            throw new InvalidArgumentException(\sprintf("The api key [%s] you privided is not valid!", $apiKey));
        }
        $this->_apiKey = $apiKey;
    }

    protected function _getClient(array $clientConfig = []) {
        if (is_null($this->_httpClient)) {
            return $this->_httpClient = new HttpClient($clientConfig);
        }
        return $this->_httpClient;
    }

    function translate($text, $lang, $srcLang = self::DEFAULT_SOURCE_LANG, $htmlFormat = true) {
        $formData = [
            'key' => $this->_apiKey,
            'lang' => $this->_formatDirection($lang, $srcLang),
            'text' => $text
        ];
        if ($htmlFormat) {
            $formData['format'] = 'html';
        }

        $response = $this->_getClient()->post($this->_url, [
            'form_params' => $formData
        ]);

        if ($response->getStatusCode() == 200) {
            $body = (string) $response->getBody();
            $json = json_decode($body);
            return $json->code == 200 ? $json->text[0] : $text;
        } else {
            return $text;
        }
    }

    function tranlateAndCache($text, $lang, $srcLang = self::DEFAULT_SOURCE_LANG, $htmlFormat = true) {
        // questo funziona crea un sistema di caching basato sul MD5 del testo tradotto
        // la traduzione viene memorizzato nel gruppo yandex
        // con chiave $srcLang.MD5($text),
        // il punto e' il separatore fisico nella stringa
        // prima di effettuare realmente la traduzione verifica che
        // la traduzione nella lingua richiesta non sia gia presente
        // se gia' presente restituisce il valore in cache
        // altrimenti effettua la traduzione e memorizza il valore
        // per usi futuri.
        $group = 'yandex';
        $item = $srcLang;
        $hash = hash('md5', $text);
    }

    protected function _formatDirection($lang, $srcLang = self::DEFAULT_SOURCE_LANG) {
        return sprintf(self::DIRECTION_FORMAT, $srcLang, $lang);
    }
}