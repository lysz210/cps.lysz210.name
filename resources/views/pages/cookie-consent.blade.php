@extends('base')

@section('page-title', 'coockies policy')

@section('main-content')
<div class="grid-container">
    <div class="grid-x">
        <div class="cell">
            <h1>@lang('cookies.title')</h1>

            <p>@lang('cookies.last-update')</p>
            
            @lang('cookies.intro')

            <h2>@lang('cookies.what-title')</h2>

            @lang('cookies.what-content')

            <h2>@lang('cookies.how-title')</h2>

            @lang('cookies.how-content')
            @lang('cookies.uses-list')

            <h2>@lang('cookies.choises-title')</h2>

            @lang('cookies.choises-content')
            
            @lang('cookies.browser-refs')

            <h2>@lang('cookies.more-title')</h2>
            @lang('cookies.more-content')
            @lang('cookies.more-list')
            
            
        </div>
    </div>
</div>
@endsection