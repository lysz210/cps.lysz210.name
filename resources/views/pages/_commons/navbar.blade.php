<div class="top-bar">
    <div class="top-bar-left">
        <ul class="dropdown menu" data-dropdown-menu>
            <li class="menu-text">@lang('cps.titolo')</li>
            <li><a href="/">Home</a></li>
            <li><a href="/faq">faq</a></li>
            <li><a href="/cookies-policy">cookies</a></li>
        </ul>
    </div>
    <div class="top-bar-right">
        <ul class="menu">
        </ul>
    </div>
</div>