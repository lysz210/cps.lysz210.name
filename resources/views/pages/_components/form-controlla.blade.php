@push('style')
#richiesta {
    margin-top: 12px;
}
@endpush
<form action="{{$action}}" method="GET" class="grid-container" id="richiesta">
    <div class="grid-x">
        <div class="cell small-12 medium-8 medium-offset-2">
            <p>Questa pagina effettua la verifica in tempo reale usando il rss del sito della questura. Il progetto prevede l'uso di <a href="https://yandex.com/">Yandex</a> per la traduzione in tutte le lingue fornite dal <a href="https://translate.yandex.com/">traduttore</a></p>
            <p>Pagina di riferimento della questura <a href="http://questure.poliziadistato.it/stranieri">questura</a></p>
            <div class="input-group">
                <input
                    class="input-group-field"
                    type="text"
                    name="pratica"
                    placeholder="Numero pratica o assicurata"
                >
                <div class="input-group-button">
                    <input type="submit" class="button" value="@lang('cps.verifica')">
                </div>
            </div>
        </div>
    </div>
</form>