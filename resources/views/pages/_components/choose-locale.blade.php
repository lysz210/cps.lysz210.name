<div class="grid-container">
    <div class="grid-x grid-margin-x">
        <h1 class="cell">Lingue disponibili</h1>
        @foreach(LaravelLocalization::getSupportedLocales() as $key => $locale)
        <a class="button shrink cell {{$key === LaravelLocalization::getCurrentLocale() ? 'active' : ''}}" href="{{LaravelLocalization::getLocalizedURL($key)}}">{{$key}}: {{$locale['native']}}</a>
        @endforeach
    </div>
</div>
<ul>
</ul>