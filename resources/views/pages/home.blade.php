@extends('base')

@section('page-title', 'Controlla il Permesso di Soggiorno')

@section('main-content')
    @component('pages._components.form-controlla', [
        'action' => '/controlla'
    ])
    @endcomponent
    @component('pages._components.choose-locale')
    @endcomponent
@endsection