@extends('pages.home')

@section('main-content')
@if($pratica)
<div class="grid-container">
    <div class="grid-x">
        <div class="cell">
            <div class="card">
                <div class="card-divider"><span>@lang('cps.pratica')</span>: <strong>{{$pratica['id']}}</strong></div>
                <div class="card-section">
                    <p>{!! $pratica['descrizione'] !!}</p>
                    <p><small>{{$pratica['data']}}</small></p>
                </div>
            </div>

        </div>
    </div>
</div>
@endif
@parent
@endsection