import Vue from 'vue'
import VueI18n from 'vue-i18n'

export async function fetchI18nCps (locale) {
    return Vue.api(`i18n/${locale}/cps`)
}
Vue.use(VueI18n)
const FALLBACK_LOCALE = 'it'
let initialLanguage = document.querySelector('html').getAttribute('lang') || FALLBACK_LOCALE

const i18n = new VueI18n({
    locale: initialLanguage,
    fallbackLocale: FALLBACK_LOCALE,
    messages: {}
})

if (initialLanguage != FALLBACK_LOCALE) {
    fetchI18nCps()
        .then(({data}) => {
            i18n.setLocaleMessage(FALLBACK_LOCALE, data)
        })
}

export default i18n