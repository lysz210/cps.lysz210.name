
export default (Vue, configs) => {
    const $store = configs.store
    Vue.prototype.$layout = {
        get drawerLeft () {
            return $store.state.layout.drawerLeft
        },
        toggleDrawerLeft () {
            console.log(this)
            $store.dispatch('layout/toggleDrawerLeft')
        }
    }
}