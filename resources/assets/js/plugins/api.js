import axios from 'axios'

export default (Vue, configs) => {
    Vue.api = Vue.prototype.$api = axios.create(configs)
}