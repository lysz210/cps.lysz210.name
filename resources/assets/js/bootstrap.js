import Vue from 'vue'
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
const headers = {
    'X-Requested-With': 'XMLHttpRequest'
}

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

const token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    headers['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

import ApiPlugin from './plugins/api'
Vue.use(ApiPlugin, {
    baseURL: '/api',
    headers
})

/**
 * add Vuetify framework for UI
 */
import Vuetify from 'vuetify'
Vue.use(Vuetify)