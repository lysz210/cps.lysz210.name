
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap'

import Vue from 'vue'

import store from './store'

import LayoutPlugin from './plugins/layout'

import router from './router'

import i18n, {fetchI18nCps} from './plugins/i18n'

router.beforeEach(async (to, from, next) => {
    let {params={}, query={}} = to
    let {
        params:fromParams={},
        query:fromQuery={}
    } = from
    let {locale=''} = params
    let {locale:fromLocale=''} = fromParams
    let {q=''} = query
    console.log(q)
    if (locale == fromLocale) {
        next()
        return
    }
    if (!i18n.availableLocales.includes(locale)) {
        let {data} = await fetchI18nCps(locale)
        i18n.setLocaleMessage(locale, data)
    }
    if (i18n.locale != locale) {
        i18n.locale = locale
        document.querySelector('html').setAttribute('lang', locale)
    }
    next()
})

Vue.use(LayoutPlugin, { store })
const app = new Vue({
    el: '#app',
    template: `<router-view></router-view>`,
    store,
    router,
    i18n
});
window.app = app
