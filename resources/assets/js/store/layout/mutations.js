export function TOGGLE_LAYOUT_LANGUAGES_DRAWER (state) {
    state.languagesDrawer = !state.languagesDrawer
}

export function SET_LAYOUT_LANGUAGES_DRAWER (state, drawerState) {
    state.languagesDrawer = drawerState
}

export function setQuesturaDrawer (state, drawerState) {
    state.questuraDrawer = drawerState
}