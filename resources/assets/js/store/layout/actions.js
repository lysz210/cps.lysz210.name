export function toggleLanguagesDrawer ({ commit, state: {languagesDrawer} }) {
    commit('TOGGLE_LAYOUT_LANGUAGES_DRAWER', !languagesDrawer)
}

export function closeLanguagesDrawer ({ commit }) {
    commit('SET_LAYOUT_LANGUAGES_DRAWER', false)
}

export function openLanguagesDrawer ({ commit }) {
    commit('SET_LAYOUT_LANGUAGES_DRAWER', true)
}

export function toggleQuesturaDrawer ({ commit, state: {questuraDrawer} }) {
    commit('setQuesturaDrawer', !questuraDrawer)
}

export function closeQuesturaDrawer ({ commit }) {
    commit('setQuesturaDrawer', false)
}

export function openQuesturaDrawer ({ commit }) {
    commit('setQuesturaDrawer', true)
}