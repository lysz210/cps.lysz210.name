<?php

use Illuminate\Database\Seeder;
use GuzzleHttp\Client as HttpClient;
use Waavi\Translation\Models\Language;

class TranslatorLanguages extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $http = new HttpClient([
            'base_uri' => 'https://translate.yandex.net/api/v1.5/tr.json/',
        ]);
        $fData = [
            'key' => config('yandex.translator.key'),
            'ui' => 'en'
        ];
        $response = $http->post('getLangs', ['form_params' => $fData]);
        if ($response->getStatuscode() == 200) {
            $body = (string)$response->getBody();
            $json = json_decode($body, true);
            $lLangs = collect(config('laravellocalization.allLocales'));
            $langs = $lLangs->intersectByKeys($json['langs'])->map(function ($item, $key) {
                $item['locale'] = $key;
                return Language::create($item);
            });
            dd($langs);
        }
    }
}
