<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendTranslotrLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('translator_languages', function (Blueprint $table) {
            $table->string('script', 10)
                ->nullable();
            $table->string('native', 60)
                ->description('Language name in native languate')
                ->nullable();
            $table->string('regional', 10)
                ->description('Regiolan code')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('translator_languages', function (Blueprint $table) {
            $table->dropColumn([
                'script',
                'native',
                'regional'
            ]);
        });
    }
}
